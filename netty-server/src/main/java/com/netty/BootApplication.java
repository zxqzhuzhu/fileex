package com.netty;

import com.netty.http.HttpServer;
import com.netty.websocket.WebSocketServer;
import com.netty.utils.Constants;

/**
 * @author gjj
 * netty-server服务器启动类
 */
public class BootApplication {


    public static void main(String[] args) {
        //启动http服务器
        new Thread(new Runnable() {
            @Override
            public void run() {
                new HttpServer().run();
            }
        }).start();
        //启动websocket服务器
        new Thread(new Runnable() {
            @Override
            public void run() {
                new WebSocketServer().run(Constants.WEBSOCKET_IP,Constants.WEBSOCKET_PORT);
            }
        }).start();
    }

}
